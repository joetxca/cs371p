// -----------
// Fri, 25 Jan
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm,   GDC 6.302
        T,  3:30-4:10pm,  Zoom
    Brian
        M,  12-1pm,       GDC 3.302
    Katherine
        T,  9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
assertions
*/

/*
Java's import is analagous to C++'s using
*/

int i = 2;
int j = 3;
cout << i << j;

(cout << i); // outputs i and returns cout

/*
output is buffered
*/

cout << "\n"; // newline only
cout << endl; // newline and flush the buffer

/*
in Java
public:    everywhere
private:   only the class
<nothing>: package
protected: package and descendents
*/

/*
in C++
public:    everywhere
private:   only the class
protected: descendents
*/

/*
Collatz Conjecture, 100 years old

take a pos int
if even, divide   by 2
if odd,  multiply by 3 and add 1
repeat until 1

5 16 8 4 2 1

cycle length of  5 is 6
cycle length of 10 is 7
*/

/*
assertions are good for preconditions and for postconditions
assertions are good for programmer errors
assertions are computational comments

assertions are not good for testing
assertions are not good for user error

unit tests are good for testing
*/
