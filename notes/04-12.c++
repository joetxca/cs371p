// -----------
// Fri, 12 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

template <typename T>
class vector {
    private:
        int _s = 0;
        T* const _a = nullptr;
    public:
        // no good
        vector (const vector& rhs) :
            _s (rhs._s),
            _a (rhs._a)
            {}
        vector (const vector& rhs) {
            if (rhs.size() != 0)
                _b = new value_type[rhs.size()];
            _e = _b + rhs.size();
            copy(rhs.begin(), rhs.end(), begin());}

        // no good
        vector& operator = (const vector& rhs) {
            _s = rhs._s;
            _a = rhs._a;
            return *this;}

void f (vector<int> z) {
    ...}

{
vector<int> x(10, 2);
vector<int> y = x;
vector<int> y(x);
vector<int> y(vector<int>(...));
f(x);
vector<int> z(20, 3);
x = z;
x.operator=(z);
}
