// -----------
// Mon, 11 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

int i;
i = 2;
++i;
cout << i; // 3

const int ci;     // no, has to be initialized
ci = 2;           // no
const int ci = 2;
++ci;             // no
cout << i;        // 2

// read-write pointer, many location

int i = 2;
const int ci = 2;
int* p;
p = i;            // no
p = &i;
++*p;
cout << i;        // 3
p = &ci;          // no

// read-only pointer, many location

int i = 2;
const int ci = 3;
const int* p;
p = &ci;
++*p;             // no
p = &i;

// read-write pointer, one location

int i = 2;
const int ci = 3;
int* const cp;       // no, has to be initialized
int* const cp = &ci; // no
int* const cp = &i;
++*cp;
int j = 3;
cp = &j;             // no

int* const cq = new int(505);
++cq;                         // no
++*cq;
delete cq;

// read-only pointer, one location

int i = 2;
const int ci = 3;
const int* const cpc;       // no, has to be initialized
const int* const cpc = &ci;
++*cpc;                     // no
cpc = &i;                   // no
const int* const cqc = &i;

// read-write reference, one location

int i = 2;
const int ci = 3;
int& r;           // no, has to be initialized
int& r = ci;      // no
int& r = i;
// most like pointer
int* const

// read-only reference, one location

int i = 2;
const int ci = 3;
const int& cr;      // no, has to be initialized
const int& cr = ci;
const int& cs = i;
// most like pointer
const int* const
