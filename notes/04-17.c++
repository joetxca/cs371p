// -----------
// Wed, 17 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
25 cells
 3 alive
22 dead

.....
..*..
..*..
..*..
.....

.....
.....
.***.
.....
.....
*/

/*
100 rows
 60 cols
6000 cells
35 live

193 generations
population explodes to 492
40 generations
populations implodes to 192
*/
