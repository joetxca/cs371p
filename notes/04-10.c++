// -----------
// Wed, 10 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

struct A {int _i;}

A()
~A()
=(A)
A(A)

template <typename T>
class vector {
    private:
        int _s = 0;
        T* const _a = nullptr;

    public:
        vector () = default;

        vector (int s) :
            _s (s),
            _a (new T[s]) {
            fill(_a, _a + _s, T());}

        vector (int s, const T& v) :
            _s (s),
            _a (new T[s]) {
            fill(_a, _a + _s, v);}

        vector (initializer_list<T> il)
            _s (il.size()),
            _a (new T[_s]) {
            copy(il.begin(), il.end(), _a);}

        T& operator [] (int i)
        const T& operator [] (int i) const

        int size () const {
            ...}

        T* begin () {
            ...}
        const T* begin () const {
            ...}

vector<int> x(10, 2);
x.reserve(20);
int* p = begin(x);
cout << *p; // 2
x.push_back(3);
cout << *p; // <undefined>
