// -----------
// Wed, 13 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

for (int i = 0; i != 10; ++i)
    s += i;

{
int i = 0;
while (i != 10) {
    s += i;
    ++i;}
}

for (int v : a)
    s += v;

auto b = begin(a);
auto e = end(a);
while (b != e) {
    int v = *b;
    s += v;
    ++b;}

/*
accumulate is a       higher-order function
    takes a binary function
transform  is another higher-order funciton
    takes a unary  function
*/

template <typename T>
struct square2  {
//    square2 () = default;
//    square2 () {}
    T operator () (const T& x) const {
        return pow(x, 2);}};

cout << int;            // no
cout << square2<int>;   // no
cout << square2<int>(); // no

cout << square2<int>(2);   // no
cout << square2<int>()(2); // 4

cout << square3<int>;   // no
cout << square3<int>(); // no

cout << square3<int>(2);    // no
cout << square3<int>()(2);  // no
cout << square3<int>(2)(2); // 4

function<int (int)>            // unary function takes and returns an int
function<bool (double, short)> // binary function takes a double and a short
                               // and returns a bool
