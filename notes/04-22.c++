// -----------
// Mon, 22 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

class Shape {
    friend bool operator != (const Shape& lhs, const Shape& rhs) {
        ...}
    ...};

class Circle : public Shape {
    friend bool operator != (const Circle& lhs, const Circle& rhs) {
        return static_cast<const Shape&>(lhs) != rhs ...
    ...
    Circle (...) :

/*
when is the initializer list required:
    1. a const
    2. a reference
    3. a derived class with a base class that does not have a default constr
*/

Shape* const p = new Circle(2, 3, 4);
p.move(...);    // no
*p.move(...);   // no
(*p).move(...);
p->move(...);

/*
in Java, dynamic binding is on by default
in C++,  static  binding is on by default (use virtual to turn it on)
*/
