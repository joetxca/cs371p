// -----------
// Mon, 15 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

template <typename T>
class vector {
    private:
        int _s = 0;
        T*  _a = nullptr;
    public:
        // no good
        vector (const vector& rhs) :
            _s (rhs._s),
            _a (rhs._a)
            {}
        // fixed
        vector (const vector& rhs) :
            _s (rhs.size()),
            _a ((rhs.size() != 0) ? new value_type[rhs.size()]) {
            copy(rhs.begin(), rhs.end(), begin());}

        // no good
        vector& operator = (const vector& rhs) {
            _s = rhs._s;
            _a = rhs._a;
            return *this;}
        // fixed
        vector& operator = (vector rhs) {
            swap(_s, rhs._s);
            swap(_a, rhs._a);
        /*
            if (this == &rhs) // (*this == rhs), wouldn't want to do that
                return *this;
            delete [] _a;
            if (rhs.size() != 0)
                _a = new value_type[rhs.size()];
            copy(rhs.begin(), rhs.end(), begin());
        */
            return *this;}

int main () {
    vector<int> x(10, 2);
    x = x;
    return 0;}

vector<int> f (...) {
    ...}

int main () {
    vector<int> x(10, 2);
    x = f(...);
    return 0;}
