// -------------------
// InitializerList.c++
// -------------------

// http://en.cppreference.com/w/cpp/utility/initializer_list

#include <algorithm>        // equal
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iostream>         // cout, endl
#include <vector>           // vector

using namespace std;

void test1 () {
    initializer_list<int> x;
    assert(x.size() == 0);}

/*
    initializer_list<int> x(); //  warning: empty parentheses interpreted as a function
*/

void test2 () {
    initializer_list<int> x{};
    assert(x.size() == 0);}

void test3 () {
    initializer_list<int> x = {};
    assert(x.size() == 0);}

/*
    initializer_list<int> x(2);  // error: no matching function for call to 'std::initializer_list<int>::initializer_list(int)'
    initializer_list<int> x = 2; // error: conversion from 'int' to non-scalar type 'std::initializer_list<int>' requested
*/

void test4 () {
    initializer_list<int> x{2};
    assert(x.size() == 1);}

void test5 () {
    initializer_list<int> x = {2};
    assert(x.size() == 1);}

void test6 () {
    initializer_list<int> x{2, 3};
    assert(x.size() == 2);}

void test7 () {
    initializer_list<int> x = {2, 3};
    assert(x.size() == 2);}

void test8 () {
    initializer_list<int> x = {2, 3, 4};
    initializer_list<int> y = x;
    assert(x.size() == 3);
    assert(y.size() == 3);
    assert(equal(begin(x), end(x), begin(y)));}

void test9 () {
    initializer_list<int> x = {2, 3, 4};
    initializer_list<int> y = {5, 6};
    assert(x.size() == 3);
    assert(y.size() == 2);
    x = y;
    assert(x.size() == 2);
    assert(y.size() == 2);
    assert(equal(begin(x), end(x), begin(y)));}

void test10 () {
    initializer_list<int> x = {2, 3, 4};
    assert(x.size() == 3);
    x = {5, 6};
    assert(x.size() == 2);
    assert(equal(begin(x), end(x), begin({5, 6})));}

void test11 () {
    initializer_list<int> x = {2, 3, 4};
    initializer_list<int> y = {2, 3, 4};
//  assert(y == x);                            //  error: no match for 'operator==' (operand types are 'std::initializer_list<int>' and 'std::initializer_list<int>')
    assert(vector<int>(x) == vector<int>(y));}

void test12 () {
    initializer_list<int> x = {2, 3, 4};
    vector<int>           y = x;
//  assert(x == y);                      //  error: no match for 'operator==' (operand types are 'std::vector<int>' and 'std::initializer_list<int>')
    assert(vector<int>(x) == y);}

void test13 () {
    initializer_list<int> x = {2, 3, 4};
    vector<int>           y;
    y = x;
    assert(vector<int>(x) == y);}

void test14 () {
    vector<int> x = {2, 3, 4};
//  assert(x == {2, 3, 4});               // error: macro "assert" passed 3 arguments, but takes just 1
    assert(x == vector<int>({2, 3, 4}));}

void test15 () {
    vector<int> x;
    x = {2, 3, 4};
    assert(x == vector<int>({2, 3, 4}));}

int main () {
    cout << "InitializerList.c++" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
    test11();
    test12();
    test13();
    test14();
    test15();
    cout << "Done." << endl;
    return 0;}
