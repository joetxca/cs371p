// -----------
// Mon,  8 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

struct A {
    explicit A (int)
    ...};

void f (A) ...

int main () {
    A x(2);
    A x = 2; // begins to fail
    f(x);
    f(A(2));
    f(2);    // begins to fail

A x(2, 3, 4);
A y(2, 3);
A z(2);
A t();        // a function declaration
A t{};
A s;

/*
what does vector need
*/

// code: 9251

template <typename T>
class vector {
    private:
    public:
        vector ()
        vector (int s)
        vector (int s, const T& v)
        vector (initializer_list<T> il)

        T& operator [] (int i)
        const T& operator [] (int i) const

        int size () const {
            ...}

        T* begin () {
            ...}
        const T* begin () const {
            ...}
