// -----------
// Wed, 27 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

int* const a = new int[s]; // an array of ints, O(1)
delete [] a;               // O(1)

T*   const b = new T[s];   // an array of Ts,   O(n), T(), s times
delete [] b;               // O(n), ~T(), s times

int* const c = new int(s); // one int, initialized to s
delete c;                  // O(1)

T*   const d = new T(s);   // one T, T(int)
delete d;                  // O(1), ~T()

/*
what could I do wrong with delete

1. misuse of the []
2. delete on invalid
3. never call delete
4. delete more than once
*/

/*
1. garbage collector
2. memory checker, Valgrind, for example
3. object-oriented types that manage the memory for you
*/

class Mammal {...};
class Tiger  {...};

int main () {
    Tiger*  p = new Mammal(...); // no
    Mammal* q = new Tiger(...);
    return 0;}

/*
casting in C, just uses parenthesis
*/

double d = 2.34;
int*   p = (int*)(&d);
cout << *p;

/*
introduces long-winded and obvious cast syntax:

1. const_cast
2. static_cast
3. reinterpret_cast
4. dynamic_cast
*/

T* a = new T[s];   // T(),  s times
fill(a, a + s, v); // =(T), s times
...
delete [] a;

{
vector<T> x(s, v); // T(T), s times
...
}
