// -----------
// Fri, 19 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

template <typename T, typename A = std::allocator<T>>
class my_vector {
    private:
        A _a;
        pointer _b;
        pointer _e;

    explicit my_vector (size_type s = 0, const_reference v = value_type(), const A& a = A()) :
        _a (a) {
        if (s != 0)
            _b = _a.allocate(s);
        _e = _b + s;
        uninitialized_fill(_a, begin(), end(), v);}

    ~my_vector () {
        destroy(_a, begin(), end());
        _a.deallocate();}

cout << "hi";

cout.operator<<("hi");

istream& operator (istream&, shape&);
