// -----------
// Mon,  6 May
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        T, 3pm,            Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

vector<CC> x(s, CC(...)); // tears down correctly
/*
AC(...), once
CC(...), once
AC(AC),  s times
CC(CC),  s times
~CC(),   once
~AC(),   once
*/

vector<AC> x(s, CC(...));
/*
doesn't compile
AC(...), once
CC(...), once
AC(...), s times
~CC(),   once
~AC(),   once
*/

vector<AC*> x(s, CC(...));     // no
vector<AC*> x(s, new CC(...));
/*
AC(...), once
CC(...), once
<nothing>
~CC(),   once
~AC(),   once
*/

vector<AC*> x(s);
fill(begin(x), end(x), new CC(...)); // still not what we want

vector<AC*> x(s); // does not tear down correctly
loop
    x[i] = new CC(...);
...
loop
    delete x[i];

{
C x = new CC(...);
/*
AC(...), once
CC(...), once
C(...),  once
~CC(),   once
~AC(),   once
*/
}
/*
~C()
~CC()
~AC()
*/

C y = new FC(...);

class C {
    private:
        AC* _p;
    public:
        C (AC* p) {
            _p = p;}
        C (const C& rhs) {
            _p = rhs._p->clone();}
        ~C () {
            delete _p;}
        C& operator = (const C& rhs) {
            ...}

{
C x = new FC(...);
C y = x;
C z = new CC(...)
y = z;
}
