// -----------
// Wed,  1 May
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        T, 3pm,            Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

struct A {
    virtual void f (int) {}};

struct B : A {
    void f (int) {}};

int main () {
    A* p = new B;
    p->f(2);      B::f(int)
    return 0;}

struct A {
    virtual void f (int) {}};

struct B : A {
    void f (int) override {}};

int main () {
    A* p = new B;
    p->f(2);      // A::f(int)
    return 0;}

struct A {
    virtual void f (int) = 0;}; // pure virtual method, abstract method

struct B : A {
    void f (int) {}};

int main () {
    A* p = new B;
    p->f(2);      // B::f(int)
    return 0;}

/*
what are the consequences of an abstract method
1. the base class becomes abstract
2. the derived class must define the method or the derived class becomes abstract
3. optional, prohibited in Java
*/

struct A {
    virtual void f (int) final {}};

struct B : A {
    // void f (int) {}};

int main () {
    A* p = new B;
    p->f(2);      // B::f(int)
    return 0;}

struct A {
    virtual void f (int) {...}       // don't do this one
    virtual void g (int) final {...}
    virtual void h (int) = 0;};

/*
what does base class communicated to the derived class:
f: optional
g: prohibited
h: required
*/

struct A {
    virtual ~A () = default;};

struct B : A {};

int main () {
    A* p = new B; // A() B()
    delete p;     // ~B() ~A()
    return 0;}

struct A {
    virtual ~A () = 0;};

A::~A () {}

struct B : A {};

int main () {
    A* p = new B; // A() B()
    delete p;     // ~B() ~A()
    return 0;}

/*
what are the consequences of an abstract destructor
1. the base class becomes abstract
*/
