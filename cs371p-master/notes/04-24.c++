// -----------
// Wed, 24 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

class Shape {
    ...}

class Circle : public Shape {
    ...}

/*
public parents are known to all
protected parents are only known to descendents
private parents are only knows to the class
*/

class  B : A {} // private
struct B : A {} // public

int main () {
    B x;
    x.f();     // no
    A* p = &x; // no
    return 0;}

class Shape {
    virtual void f () {...}};

class Circle : public Shape {
    friend bool operator == (const Circle& lhs, const Circle& rhs) {
        return (static_cast<const Shape&>(lhs) == rhs) && (lhs._r == rhs._r)
    private:
        ...
    public:
        Circle (int x, int y, int r) :
            Shape(x, y) {
            _r = r;}

        void f () {
            Shape::f();
            ...}

struct A {
    int i;
    virtual void f (int) {}};

struct B : A {
    int j;
    void f (int) {}}

struct C : A {
    int k;
    void f (int) {}}

/*
virtual method result in a pointer being stored in all instances of A, B, and C
those pointers point to a virtual function table
the table is full of pointers to methods
*/

/*
footprint of an A is
int
void*, pointer to virtual function table

footprint of a B or a C
int
void*, pointer to virtual function table
int
*/

int main () {
    A* p;
    if (...)
        p = new B;
    else
        p = new C;
    p->f(2);       // dynamically bound, can't inline
    p->A::f(2);    // statically  bound, can   inline

    B x;
    x.f(2);        // statically  bound, can   inline
    return 0;}

struct A {
    A () {
        f();}                 // statically bound, can inline
    ~A () {
        f();}                 // statically bound, can inline
    virtual void f () {...}};

struct B : A {
    void f () {...}};

int main () {
    A* p = new B; // A(),  B()
    delete p;     // ~B(), ~A()
    return 0;}
