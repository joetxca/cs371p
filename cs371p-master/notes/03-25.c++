// -----------
// Mon, 25 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
allocator
    constructor
    allocate
    deallocate
    construct (already done)
    destroy (already done)
    [] (not in the real allocator)
    iterator (not in the real allocator)
    valid (not in the real allocator)
*/

/*
managing an array of size N bytes
*/

template <typename T, typename N>
class my_allocator {
    private:
        char _a[N];

my_allocator<int, 100> x;

/*
managing a series of blocks
a block
    sentinel (int)
    data
    sentinel (int)
the sentinels are positive for free blocks
negative for busy blocks
the values of the sentinels is the number bytes of data
*/

my_allocator<double, 100> x;

/*
constructor for a 100 byte allocator
92...92
*/

double* a = x.allocate(5);

/*
return the address of the first double
-40...-40+44...+44
*/

double* b = x.allocate(3); // O(n)

/*
48 bytes 32 bytes 20 bytes  = 100 bytes
-40...-40-24...-24+12...+12
*/

/*
first fit
*/

x.deallocate(b); // O(1)

/*
48 bytes 32 bytes 20 bytes  = 100 bytes
-40...-40+24...+24+12...+12
*/
