// -----------
// Fri,  3 May
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        T, 3pm,            Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

struct A {
    virtual A* clone () const = 0;};

struct B : A {
    B* clone () const {
        return new B(*this);}};

/*
return types are covariant
*/

int main () {
    A* p = new B(...);
    A* q = p->clone();

    B* p = new B(...);
    B* q = p->clone();
