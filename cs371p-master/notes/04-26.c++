// -----------
// Fri, 26 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

struct A {
    void f (int) {}};

struct B : A {
    void f (int) {}};

int main () {
    B x;
    x.f(2);    // B::f(int)
    return 0;}

struct A {
    void f (int) {}};

struct B : A {
    void f (string) {}};

int main () {
    B x;
    x.f(2);    // not compile
    x.A::f(2);
    return 0;}

struct A {
    void f (int) {}};

struct B : A {
    void f (int i) {A::f(i);}
    void f (string) {}};

int main () {
    B x;
    x.f(2);
    return 0;}

struct A {
    void f (int) {}};

struct B : A {
    using A::f;
    void f (string) {}};

int main () {
    B x;
    x.f(2);
    return 0;}

class Shape {
    ...
    virtual bool equals (const Shape& rhs) const {...}
    ...};

class Circle : public Shape {
    ...
    bool equals (const Shape& rhs) const {...}
    ...};
