// ---------
// Stack.c++
// ---------

// http://en.cppreference.com/w/cpp/container/stack

#include <cassert>  // assert
#include <deque>    // deque
#include <iostream> // cout, endl
#include <stack>    // stack
#include <vector>   // vector

using namespace std;

void test1 () {
    stack<int> x;
    assert(x.empty());
    assert(x.size() == 0);}

void test2 () {
    stack<int> x;
    x.push(2);
    x.push(3);
    x.push(4);
    assert(!x.empty());
    assert(x.size() == 3);
    assert(x.top()  == 4);}

void test3 () {
    stack<int> x;
    x.push(2);
    x.push(3);
    x.push(4);
    x.pop();
    assert(x.size() == 2);
    assert(x.top()  == 3);}

void test4 () {
    stack<int> x;
    x.push(2);
    x.push(3);
    x.push(4);
    x.top() = 5;
    assert(x.top() == 5);
    const stack<int>& r = x;
//  r.top() = 6;             // top() on a const stack must return an r-value
    assert(r.top() == 5);}

void test5 () {
    stack<int> x;
    x.push(2);
    x.push(3);
    x.push(4);
    stack<int> y = x;
    assert(x == y);}

void test6 () {
    stack<int> x;
    x.push(2);
    x.push(3);
    x.push(4);
    stack<int> y;
    x.push(5);
    x.push(6);
    stack<int>& z = (x = y);
    assert(x  == y);
    assert(&x == &z);}

void test7 () {
    stack<int, vector<int>> x;
    x.push(2);
    x.push(3);
    x.push(4);
    x.pop();
    assert(x.size() == 2);
    assert(x.top()  == 3);}

void test8 () {
    vector<int> c;
//  stack<int, vector<int>> x = c; // stack's container constructor must be explicit
    stack<int, vector<int>> x(c);
    x.push(2);
    x.push(3);
    x.push(4);
    x.pop();
    assert(x.size() == 2);
    assert(x.top()  == 3);}

int main () {
    cout << "Stack.c++" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    cout << "Done." << endl;
    return 0;}
